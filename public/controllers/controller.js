var myApp= angular.module("ContactListApp",[]);

myApp.controller('AppCtrl',['$scope','$http',function($scope,$http){
        console.log('Controller initialized');

        person1 = {
                name:'Pablo',
                email:'pablo@aws.us.es',
                number:'123 456 433'
        };

        person2 = {
                name:'Antonio',
                email:'antonio@aws.us.es',
                number:'444 222 123'
        };

        var contactlist = [person1,person2];
        $scope.contactlist = contactlist;

}]);
